#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2024] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Remove Redundant - An attempt to remove objects that do not contribute to
# The overall appearance of the SVG (Does not account for filters)
# An Inkscape 1.2.1+ extension
##############################################################################

import inkex


import gi
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf

import sys, os
import shutil

import tempfile

from uuid import uuid4

from copy import deepcopy


def make_temp_folder(self):
    """
    Creates a temp folder to which files can be written \n
    To remove folder at end of script use: \n
    # Cleanup temp folder \n
    if hasattr(self, 'inklin_temp_folder'):
        shutil.rmtree(self.inklin_temp_folder)

    :return: A temp folder path string
    """

    temp_folder = tempfile.mkdtemp()
    self.inklin_temp_folder = temp_folder
    return temp_folder

def make_svg_temp_file(self, svg, extension='.svg'):
    import time
    temp_file_name = str(time.time()).replace('.', '') + extension
    if hasattr(self, 'inklin_temp_folder'):
        temp_folder = self.inklin_temp_folder
    else:
        temp_folder = make_temp_folder(self)
    temp_filepath = (os.path.join(temp_folder, temp_file_name))
    with open(temp_filepath, 'w') as output_svg:
        output_svg.write(svg.tostring().decode('utf-8'))
    return temp_filepath

def svg_file_object_to_path(self, svg):

    temp_svg_filepath = make_svg_temp_file(self, svg)

    object_to_path_svg_filepath = os.path.join(self.inklin_temp_folder, 'otp.svg')

    # inkex.errormsg(object_to_path_svg_filepath)

    action_sting = f'--actions="select-all;object-to-path;export-filename:{object_to_path_svg_filepath};export-do;"'

    inkex.command.inkscape(temp_svg_filepath, action_sting)

    otp_svg = inkex.load_svg(object_to_path_svg_filepath).getroot()

    return otp_svg


class SpOverlaps:


    def element_to_pixbuf(self, svg_element, element):

        # Lets try to fix errors at very small values

        pixel_scale_bool = True

        if pixel_scale_bool:

            bbox = element.bounding_box()

            min_x = bbox.left
            min_y = bbox.top
            width = bbox.width
            height = bbox.height

            view_box_list = [min_x, min_y, width, height]
            view_box_string_list = [str(x) for x in view_box_list]
            view_box = ' '.join(view_box_string_list)
            # element.set('viewBox', view_box)
            svg_element.set('viewBox', view_box)

            page_bbox = self.svg.get_page_bbox()
            page_width = page_bbox.width
            page_height = page_bbox.height
            # We want max width of 1000 pixels
            aspect_ratio = page_width / page_height
            new_page_width = self.options.accuracy_int
            new_page_height = new_page_width / aspect_ratio
            svg_element.set('width', f'{new_page_width}px')
            svg_element.set('height', f'{new_page_height}px')


        element = svg_element.tostring()

        loader = GdkPixbuf.PixbufLoader()
        loader.write(element)
        loader.close()
        pixbuf = loader.get_pixbuf()
        return pixbuf

    def compare_pixbuf_pixels(self, svg, element, consider_opacity=False):

        svg_copy = deepcopy(svg)

        svg_copy_element = svg_copy.getElementById(element.get_id())

        if consider_opacity:
            svg_copy_element.set('opacity', 0)
        else:
            svg_copy_element.getparent().remove(svg_copy_element)

        element_pixbuf = SpOverlaps.element_to_pixbuf(self, svg_copy, element)
        master_pixbuf = SpOverlaps.element_to_pixbuf(self, svg, element)


        # random_file_name = str(uuid4()) + '__master_pixbuf.png'
        # master_pixbuf.savev(os.path.join(temp_folder_path, random_file_name), 'png')

        # random_file_name = str(uuid4()) + f'{element.get_id()}_pixbuf.png'
        # element_pixbuf.savev(os.path.join(SpOverlaps.temp_folder_path, random_file_name), 'png')

        element_pixbuf_pixels = element_pixbuf.get_pixels()
        master_pixbuf_pixels = master_pixbuf.get_pixels()

        self.count += 1


        if element_pixbuf_pixels == master_pixbuf_pixels:
            return True
        else:
            # random_file_name = str(self.count) + '__master_pixbuf.png'
            # master_pixbuf.savev(os.path.join(SpOverlaps.temp_folder_path, random_file_name), 'png')
            #
            # random_file_name = str(self.count) + '__element_pixbuf.png'
            # element_pixbuf.savev(os.path.join(SpOverlaps.temp_folder_path, random_file_name), 'png')

            return False



    def compare_pixbufs(self, svg, element, consider_opacity=False):

        if consider_opacity:
            # First lets check if there is a complete underlap
            if not SpOverlaps.compare_pixbuf_pixels(self, svg, element, consider_opacity=False):
                return False
            else:
                return SpOverlaps.compare_pixbuf_pixels(self, svg, element, consider_opacity=True)
        else:
            return SpOverlaps.compare_pixbuf_pixels(self, svg, element, consider_opacity=False)



class RemoveRedundant(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--object_source_radio", type=str, dest="object_source_radio", default='selected')

        pars.add_argument("--accuracy_int", type=int, dest="accuracy_int", default=100)
    
    def effect(self):

        self.count = 0

        make_temp_folder(self)

        temp_folder_path = os.path.join(self.inklin_temp_folder, 'pixbuff')

        SpOverlaps.temp_folder_path = temp_folder_path

        if os.path.isdir(temp_folder_path):
            shutil.rmtree(temp_folder_path)
        
        os.mkdir(temp_folder_path)

        svg_copy = deepcopy(self.svg)

        otp_svg = svg_file_object_to_path(self, svg_copy)

        svg_crisp = otp_svg

        svg_crisp.set('shape-rendering', 'crispEdges')

        if self.options.object_source_radio == 'selected':
            selection_list = self.svg.selected
            drawable_items = selection_list
            if len(selection_list) < 1:
                sys.exit()
        else:
            drawable_items = svg_crisp.xpath('//svg:circle | //svg:ellipse | //svg:line | //svg:path | //svg:text | //svg:polygon | //svg:polyline | //svg:rect | //svg:use | //svg:image')

        # A list to hold the items we are going to delete
        deletion_list = []

        for element in drawable_items:

            underlap_bool = SpOverlaps.compare_pixbufs(self, svg_crisp, element, consider_opacity=True)

            if underlap_bool:
                deletion_list.append(element)


        for underlap_element in deletion_list:

            self.svg.getElementById(underlap_element.get_id()).delete()

        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)


if __name__ == '__main__':
    RemoveRedundant().run()
